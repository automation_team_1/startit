require 'capybara/dsl'
include Capybara::DSL

Given(/^I am on the home page$/) do
  visit 'https://www.dropbox.com/'
end

Given(/^I navigate to sign in page$/) do
  find( '#sign-in' ).click
end

When(/^I fill in login field with username$/) do
  fill_in 'login_email', with: 'login@mailbox.com'
end

When(/^I fill in password field with password$/) do
  fill_in 'login_password', with: 'password'
end

When(/^I click sign in button$/) do
  find( '#index-sign-in-modal .login-form .login-button.button-primary' ).click
end

Then(/^I should be signed in$/) do
  expect(page).to have_css( '#account-header' )
end