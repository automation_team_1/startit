Feature: Login
	In order to have access to dropbox
	As a User
	I want to sign in

Scenario: Login
	Given I am on the home page
	And I navigate to sign in page
	When I fill in login field with username
	And I fill in password field with password
	And I click sign in button
	Then I should be signed in